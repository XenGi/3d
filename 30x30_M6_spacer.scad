$fn = 100;

width = 30;
depth = 30;
height = 10;
radius = 5;

difference() {
  /*
  hull() {
    cylinder(h=height, r=radius);
    translate([width-2*radius,0,0]) cylinder(h=height, r=radius);
    translate([0,depth-2*radius,0]) cylinder(h=height, r=radius);
    translate([width-2*radius,depth-2*radius,0]) cylinder(h=height, r=radius);
  }
  */
  cylinder(h=height, d=depth);
  //translate([width/2-radius,depth/2-radius,0]) cylinder(h=height, r=6/2);
  cylinder(h=height, r=6/2);
}
