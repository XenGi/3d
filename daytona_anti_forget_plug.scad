$fn = 64;

nut_d = 15.2;
nut_h = 15;
diameter = 2*nut_d;
height = 10;


intersection() {
  union() {
    cylinder(h=nut_h, d=nut_d);
    translate([0,0,nut_h]) {
      difference() {
        cylinder(h=height, d=diameter);
        for (i=[1:60:360]) {
          rotate(i, [0,0,1]) translate([diameter/2,0,height]) sphere(d=diameter/3+2);
        }
        translate([0,0,height+diameter/2-2.5]) sphere(d=diameter);
      }
    }
  }
  translate([0,0,7]) sphere(d=1.5*diameter);
}
