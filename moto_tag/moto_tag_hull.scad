$fn = 64;

diameter = 31.9;
height = 8;

difference() {
  hull() {
    translate([-35/2+5,0,0]) rotate([270,0,0]) cylinder(h=35/2,r=5);
    translate([35/2-5,0,0]) rotate([270,0,0]) cylinder(h=35/2,r=5);
  }
  scale([1.05,1.05,1.05]) union() {
    rotate_extrude(convexity=10, $fn=100) translate([diameter/2-2, 0, 0]) circle(r=2, $fn=100);
    cylinder(h=2,r=diameter/2-2);
    cylinder(h=height/2, r1=diameter/2, r2=10.5);
    rotate([0,180,0]) cylinder(h=height/2, r1=diameter/2, r2=10.5);
  }
}
