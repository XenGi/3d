$fn = 64;

diameter = 31.9;
height = 8;

rotate_extrude(convexity=10, $fn=100) translate([diameter/2-2, 0, 0]) circle(r=2, $fn=100);
cylinder(h=2,r=diameter/2-2);
cylinder(h=height/2, r1=diameter/2, r2=10.5);
rotate([0,180,0]) cylinder(h=height/2, r1=diameter/2, r2=10.5);
