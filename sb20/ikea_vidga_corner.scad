// ********************
// Configuration
 // ********************
angle = 45;
radius = 160;
ends = 40; // Length of the straight parts on the ends
layer_height = 0.15;


// ********************
// Internals
// ********************
$fn = 128;
width = 15;
height = 22;
walls = 1.2;
opening = 9;
middle = 13;

// 2D profile of the rail
module profile() {
  union() {
    difference() {
      square([width, height], center=true);
      square([width-2*walls, height-2*walls], center=true);
      square([opening, height], center=true);
    }
    translate([0,middle-height/2,0])
      square([width, walls], center=true);
    translate([0,height/2-walls,0])
      square([width, layer_height], center=true);
  }
}

// angled part
rotate_extrude(angle=angle, convexity=10)
  translate([radius,0,0])
  profile();

// straight start
translate([radius,0,0])
  rotate([90,0,0])
  linear_extrude(ends)
  profile();

// straight end
rotate([0,0,angle])
  translate([radius,ends,0])
  rotate([90,0,0])
  linear_extrude(ends)
  profile();


translate([153.7,-20,2.6]) rotate([90,0,0]) connector();
translate([57.4,160,2.6]) rotate([90,0,45]) connector();

module connector() {
  height=7.0;
  width=12.6;
  length=50;

  difference() {
    cube([width,height,length]);
    translate([width/2,0,10]) rotate([270,0,0]) cylinder(h=height, d=width/2);
    translate([width/2,0,length-10]) rotate([270,0,0]) cylinder(h=height, d=width/2);
  }
}
