$fn = 64;

width = 50;
height = 70;
thickness = 3;
hole = 3;

difference() {
  union() {
    cube([height, width, thickness]);
    cube([thickness, width, height]);
    translate([0,width/2-thickness/2,0]) hull() {
      cube([height/2, thickness, thickness]);
      cube([thickness, thickness, height/2]);
    }
    intersection() {
      rotate([270,0,0]) cylinder(h=width, r=thickness*4);
      cube([thickness*4, width, thickness*4]);
    }
  }
  translate([height/1.55,(width/4)*3,0]) cylinder(h=thickness, r1=hole, r2=hole+2);
  translate([height/1.55,width/4,0]) cylinder(h=thickness, r1=hole, r2=hole+2);
  translate([height/1.15,(width/4)*3,0]) cylinder(h=thickness, r1=hole, r2=hole+2);
  translate([height/1.15,width/4,0]) cylinder(h=thickness, r1=hole, r2=hole+2);
  
  translate([0,width/4,height/1.55]) rotate([0,90,0]) cylinder(h=thickness, r1=hole, r2=hole+2);
  translate([0,(width/4)*3,height/1.55]) rotate([0,90,0]) cylinder(h=thickness, r1=hole, r2=hole+2);
  translate([0,width/4,height/1.15]) rotate([0,90,0]) cylinder(h=thickness, r1=hole, r2=hole+2);
  translate([0,(width/4)*3,height/1.15]) rotate([0,90,0]) cylinder(h=thickness, r1=hole, r2=hole+2);
  
  hull() {
    translate([0,0,-thickness]) cube([thickness*2, width, thickness]);
    translate([-thickness,0,0]) cube([thickness, width, thickness*2]);
  }
}
