$fn=64;

radius=8;
height=5;
width=30;
length=50;
wall=3;

difference() {
  hull() {
    cylinder(h=height,r=radius+wall);
    translate([width,length/2,0]) cylinder(h=height,r=radius+wall);
    translate([width,-length/2,0]) cylinder(h=height,r=radius+wall);
  }
  cylinder(h=height,r=radius);
  translate([-radius-wall-4,-length/2,0]) cube([radius+wall,length,height]);
  translate([width,0,0]) cylinder(h=height,r=radius);
  translate([width,length/2,0]) cylinder(h=height,r=radius);
  translate([width,-length/2,0]) cylinder(h=height,r=radius);
}
