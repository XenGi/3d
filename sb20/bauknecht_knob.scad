$fn=64;

extend=true;

knob_diameter=33.5;
knob_height=32;

shaft_height=extend ? 73 : 55;
shaft_diameter=12;

connector_diameter=5.5;
connector_height=30;
connector_width=7;

walls=2;

cylinder(h=knob_height, d=knob_diameter);
difference() {
  cylinder(h=shaft_height, d=shaft_diameter);
  translate([0,0,shaft_height-connector_height]) {
    cylinder(h=connector_height, d=connector_diameter);
    translate([-connector_width/2,0,0]) cube([connector_width,connector_diameter/2,connector_height]);
  }
}

if (extend) translate([0,0,73-55]) scale([1,1,0.1]) sphere(d=knob_diameter+1);
