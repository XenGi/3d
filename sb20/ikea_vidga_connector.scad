$fn=64;

height=7;
width=12;
length=50;

difference() {
  cube([width,height,length]);
  translate([width/2,0,10]) rotate([270,0,0]) cylinder(h=height, d=width/2);
  translate([width/2,0,length-10]) rotate([270,0,0]) cylinder(h=height, d=width/2);
}
