$fn = 64;

height = 2;
diameter = 28;

difference() {
  cylinder(h=height, d=diameter);
  cylinder(h=height, d=10);
  cube([diameter, 2, 1], center=true);
  rotate([0,0,180/3]) cube([diameter, 2, 1], center=true);
  rotate([0,0,-180/3]) cube([diameter, 2, 1], center=true);
  cylinder(h=1, d=17, center=true);
};
