$fn = 64;

height = 1.5;
diameter = 80;
hole_diameter = 60;


difference() {
  cylinder(h=height, r1=diameter/2, r2=diameter/2-2);
  translate([hole_diameter/2,0,0]) cylinder(h=height, r1=2, r2=3);
  translate([-hole_diameter/2,0,0]) cylinder(h=height, r1=2, r2=3);
  cylinder(h=height, d=12);
};
