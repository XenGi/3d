$fn = 64;

height=20;
width=20;
thickness=3;
hole=2;

hull() {
  translate([0,-thickness/2,0]) cube([3*height/4,thickness,thickness]);
  rotate([0,90,0]) translate([-(3*height/4),-thickness/2,0]) cube([3*height/4,thickness,thickness]);
}

difference() {
  hull() {
    translate([0,-width/2,0]) cube([thickness,width,thickness]);
    translate([height,0,0]) cylinder(h=thickness,d=width/2);
  }
  translate([height,0,0]) cylinder(h=thickness,d1=hole,d2=2*hole);
}

translate([thickness,0,0]) rotate([0,-90,0]) difference() {
  hull() {
    translate([0,-width/2,0]) cube([thickness,width,thickness]);
    translate([height,0,0]) cylinder(h=thickness,d=width/2);
  }
  translate([height,0,0]) cylinder(h=thickness,d1=2*hole,d2=hole);
}
