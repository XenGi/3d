$fn = 64;

width = 440;
height = 650;
depth = 170;
thickness = 1.5;

wood_width = 25;
wood_height = 300;
wood_spacing = 5;

standoff_width = 10;
standoff_height = 100;
standoff_depth = 5;

cube([width, thickness, height]);
cube([thickness, depth, height]);

// standoffs front

for (i = [0:14]) {
  translate([i*(wood_width+wood_spacing)+wood_width/2-standoff_width/2-wood_spacing, -standoff_depth, wood_height/2-standoff_height/2]) cube([standoff_width, standoff_depth, standoff_height]);
}

for (i = [0:14]) {
  translate([i*(wood_width+wood_spacing)+wood_width/2-standoff_width/2-wood_spacing, -standoff_depth, wood_height+wood_height/2-standoff_height/2]) cube([standoff_width, standoff_depth, standoff_height]);
}

for (i = [0:14]) {
  translate([i*(wood_width+wood_spacing)+wood_width/2-standoff_width/2-wood_spacing, -standoff_depth, 2*wood_height+50/2-25/2]) cube([standoff_width, standoff_depth, 25]);
}