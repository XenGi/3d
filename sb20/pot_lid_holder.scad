$fn = 128;

depth = 13;
wall = 3;

outer_radius = 250/2;
inner_radius = 240/2;

intersection() {
  difference() {
    union() {
      cylinder(h=depth+wall, r=outer_radius);
      // screw socket
      translate([-0.71*outer_radius, -0.71*outer_radius, 0]) cylinder(h=depth+wall, r=10);
      translate([0.71*outer_radius, -0.71*outer_radius, 0]) cylinder(h=depth+wall, r=10);
      translate([-0.71*outer_radius, 0.71*outer_radius, 0]) cylinder(h=depth+wall, r=10);
      translate([0.71*outer_radius, 0.71*outer_radius, 0]) cylinder(h=depth+wall, r=10);
    }
    // screw hole
    translate([-0.71*outer_radius, -0.71*outer_radius, 0]) cylinder(h=depth+wall, r=3);
    translate([0.71*outer_radius, -0.71*outer_radius, 0]) cylinder(h=depth+wall, r=3);
    translate([-0.71*outer_radius, 0.71*outer_radius, 0]) cylinder(h=depth+wall, r=3);
    translate([0.71*outer_radius, 0.71*outer_radius, 0]) cylinder(h=depth+wall, r=3);
    // screw hole indent
    translate([0,0,depth+1]) {
      translate([-0.71*outer_radius, -0.71*outer_radius, 0]) cylinder(h=wall-1, r=5);
      translate([0.71*outer_radius, -0.71*outer_radius, 0]) cylinder(h=wall-1, r=5);
      translate([-0.71*outer_radius, 0.71*outer_radius, 0]) cylinder(h=wall-1, r=5);
      translate([0.71*outer_radius, 0.71*outer_radius, 0]) cylinder(h=wall-1, r=5);
    }
    cylinder(h=depth, r=inner_radius);
    translate([0,0,(depth+wall)/2]) cube([outer_radius, 2*outer_radius, depth+wall], center=true);
    translate([0,0,(depth+wall)/2]) cube([2*outer_radius, outer_radius, depth+wall], center=true);
  }
  // rounded corners
  union() {
    translate([-0.68*outer_radius, -0.68*outer_radius, 0]) cylinder(h=depth+wall, r=25);
    translate([0.68*outer_radius, -0.68*outer_radius, 0]) cylinder(h=depth+wall, r=25);
    translate([-0.68*outer_radius, 0.68*outer_radius, 0]) cylinder(h=depth+wall, r=25);
    translate([0.68*outer_radius, 0.68*outer_radius, 0]) cylinder(h=depth+wall, r=25);
  }
}