$fn = 64;

big_radius = 120/2;
thin_radius = 90/2;
wall_thickness = 5;
screw_size = 5;


difference() {
  union() {
    translate([-wall_thickness,-wall_thickness,0]) rotate_extrude(angle=90, convexity=10) translate([big_radius+wall_thickness, 0, 0]) circle(r=big_radius+wall_thickness);
    translate([-wall_thickness,-wall_thickness,0]) rotate([0,270,0]) translate([0,big_radius+wall_thickness,0]) cylinder(h=50, r1=big_radius+wall_thickness, r2=thin_radius+wall_thickness);
    translate([-wall_thickness,-wall_thickness,0]) rotate([0,90,90]) linear_extrude(big_radius+wall_thickness) polygon(points=[[big_radius+wall_thickness,0], [-big_radius-wall_thickness,0], [-thin_radius-wall_thickness,50], [thin_radius+wall_thickness,50]]);
    translate([-wall_thickness, -wall_thickness, 0]) difference() {
      cylinder(h=2*(big_radius+wall_thickness),r=big_radius+wall_thickness, center=true);
      translate([-big_radius-wall_thickness,-big_radius-wall_thickness,-big_radius-wall_thickness]) cube([big_radius+wall_thickness, 2*(big_radius+wall_thickness), 2*(big_radius+wall_thickness)]);
      translate([0,-big_radius-wall_thickness,-big_radius-wall_thickness]) cube([big_radius+wall_thickness, big_radius+wall_thickness, 2*(big_radius+wall_thickness)]);
    }
  }
  rotate_extrude(angle=90, convexity = 10) translate([big_radius, 0, 0]) circle(r=big_radius);
  rotate([90,0,0]) translate([big_radius,0,0]) cylinder(h=wall_thickness, r=big_radius);
  rotate([0,270,0]) translate([0,big_radius,0]) cylinder(h=50, r1=big_radius, r2=thin_radius);
  rotate([0,270,0]) translate([0,big_radius,50]) cylinder(h=wall_thickness, r=thin_radius);  
}

// front screw hole
translate([2*big_radius+wall_thickness+2*screw_size-5,0,0]) {
  rotate([90,0,0]) difference() {
    union() {
      translate([-2*screw_size,-screw_size,0]) cube([2*screw_size, 2*screw_size, wall_thickness]);
      cylinder(h=wall_thickness, d=2*screw_size);
    }
    cylinder(h=wall_thickness, d=screw_size);
  }
}

// left screw hole
translate([0,0,-big_radius-wall_thickness-2*screw_size+2]) {
  rotate([90,90,0]) difference() {
    union() {
      translate([-2*screw_size,-screw_size,0]) cube([2*screw_size, 2*screw_size, wall_thickness]);
      cylinder(h=wall_thickness, d=2*screw_size);
    }
    cylinder(h=wall_thickness, d=screw_size);
  }
}

// right screw hole
translate([0,0,big_radius+wall_thickness+2*screw_size-2]) {
  rotate([90,270,0]) difference() {
    union() {
      translate([-2*screw_size,-screw_size,0]) cube([2*screw_size, 2*screw_size, wall_thickness]);
      cylinder(h=wall_thickness, d=2*screw_size);
    }
    cylinder(h=wall_thickness, d=screw_size);
  }
}
