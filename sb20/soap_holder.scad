// fits 500ml variant: https://www.amazon.de/dp/B0CH364BPP

$fn = 64;

radius = 30;
width = 60;
length = 130-2*radius+5;
wall = 2;
screw_distance = 207;


color("lime") translate([-length/2-radius-wall/2,0,0]) difference() {
  hull() {
    translate([length/2,0,0]) rotate([270,0,0]) cylinder(h=width+2*wall, r=radius+wall);
    translate([-length/2,0,0]) rotate([270,0,0]) cylinder(h=width+2*wall, r=radius+wall);
  }
  hull() {
    translate([length/2,wall,0]) rotate([270,0,0]) cylinder(h=width, r=radius);
    translate([-length/2,wall,0]) rotate([270,0,0]) cylinder(h=width, r=radius);
  }
  translate([-length/2-radius-wall,wall,0]) cube([length+2*radius+2*wall, width, radius+wall]);
  translate([0,width/2,-radius-wall]) cylinder(h=wall, r=5);
}

color("yellow") translate([length/2+radius+wall/2,0,0]) difference() {
  hull() {
    translate([length/2,0,0]) rotate([270,0,0]) cylinder(h=width+2*wall, r=radius+wall);
    translate([-length/2,0,0]) rotate([270,0,0]) cylinder(h=width+2*wall, r=radius+wall);
  }
  hull() {
    translate([length/2,wall,0]) rotate([270,0,0]) cylinder(h=width, r=radius);
    translate([-length/2,wall,0]) rotate([270,0,0]) cylinder(h=width, r=radius);
  }
  translate([-length/2-radius-wall,wall,0]) cube([length+2*radius+2*wall, width, radius+wall]);
  translate([0,width/2,-radius-wall]) cylinder(h=wall, r=5);
}

color("white") difference() {
  translate([-radius,wall,-radius-wall]) cube([2*radius, width, radius]);
  translate([-radius,wall,0]) rotate([270,0,0]) cylinder(h=width, r=radius);
  translate([radius,wall,0]) rotate([270,0,0]) cylinder(h=width, r=radius);
}

color("white") {
  difference() {
    hull() {
      translate([screw_distance/2, width+wall, 1.5*length]) rotate([270,0,0]) cylinder(h=3, d=10);
      translate([screw_distance/2, width+wall, 0]) rotate([270,0,0]) cylinder(h=3, d=20);
    }
    translate([screw_distance/2, width+wall, 1.5*length]) rotate([270,0,0]) cylinder(h=3, d=3);
  }
  difference() {
    hull() {
      translate([-screw_distance/2, width+wall, 1.5*length]) rotate([270,0,0]) cylinder(h=3, d=10);
      translate([-screw_distance/2, width+wall, 0]) rotate([270,0,0]) cylinder(h=3, d=20);
    }
    translate([-screw_distance/2, width+wall, 1.5*length]) rotate([270,0,0]) cylinder(h=3, d=3);
  }
}