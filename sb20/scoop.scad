$fn = 64;

radius = 25;
height = 45;
wall=1.2;

difference() {
  union() {
    hull() {
      cylinder(h=height+wall, r=radius+wall);
      translate([radius*1.2,0,height]) cylinder(h=wall, r=8);
    }
    difference() {
      hull() {
        translate([-radius-12,0,height]) cylinder(h=wall, r=radius/2);
        translate([0,0,height]) cylinder(h=wall, r=radius/2);
      }
      translate([-radius-12,0,height]) cylinder(h=wall, r=radius/3.5);
    }
  }
  translate([0,0,wall]) hull() {
    cylinder(h=height, r=radius);
    translate([radius*1.15,0,height]) cylinder(h=wall, r=8-wall/2);
  }
}
