$fn=64;

height=2;
corner_radius=2;
width=47.5;
hole_radius=8.3;
hole_margin=11.75;

difference() {
  hull() {
    cylinder(h=height, r=corner_radius);
    translate([width-2*corner_radius,0,0]) cylinder(h=height, r=corner_radius);
    translate([0,width/2-2*corner_radius,0]) cylinder(h=height, r=corner_radius);
    translate([width-2*corner_radius,width/2-2*corner_radius,0]) cylinder(h=height, r=corner_radius);
  }
  translate([hole_margin-corner_radius,hole_margin-corner_radius,0]) cylinder(h=height,r=hole_radius);
  translate([width-hole_margin-corner_radius,hole_margin-corner_radius,0]) cylinder(h=height,r=hole_radius);
}
