/* Adapter for turn signals of a Triumph Daytona 675
 *
 * by XenGi (2022)
 */

$fn = 64;

height = 8;
width = 18;
inner_height = 4;
inner_width = 12;
length = 36;
screw_hole = 8;
signal_hole = 10;

difference() {
    hull() {
        cylinder(h=height, r=width/2);
        translate([length-width,0,0]) cylinder(h=height, r=width/2);
    };
    hull() {
        translate([length-inner_width-(length-width)/2,0,inner_height]) cylinder(h=height-inner_height, r=inner_width/2);
        translate([length-inner_width,0,inner_height]) cylinder(h=height-inner_height, r=inner_width/2);
    };
    translate([length-width,0,0]) cylinder(h=height, d=screw_hole);
    cylinder(h=height, d=signal_hole);
};

