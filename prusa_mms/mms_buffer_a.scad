$fn = 64;

include <panel.scad>

ref="R4";
radius = 100;
wall = 2;
thickness = 1;
height = 5*10+6*thickness;  // 5 chambers plus 6 walls
mouth_depth = 5;

difference() {
  panel(height, radius, thickness);
  translate([0,-5,thickness/2]) linear_extrude(thickness/2) text(ref, size=10, halign="center");
  translate([0,0,thickness]) {
    cylinder(h=height, r=radius-wall);
    translate([25, 25, 0]) cube([radius, radius, height]);
    translate([-25, 25, 0]) rotate([0,0,90]) cube([radius, radius, height]);
    translate([-radius/2-15, -25, 0]) rotate([0,0,180]) cube([radius, radius, height]);
    translate([radius/2+15, -25, 0]) rotate([0,0,270]) cube([radius, radius, height]);    
  }
  translate([-radius,-radius-87,0]) cube([2*radius, radius, height]);
  
    // teeth
  for ( i = [1:1:4] ) {
    translate([0,0,i*(thickness+10)]) {
      translate([-radius,-5,0]) cube([2*wall, 10, 1]);
      translate([radius-2*wall,-5,0]) cube([2*wall, 10, 1]);
      translate([-5,radius-2*wall,0]) cube([10, 2*wall, 1]);
    }
  }
}

// mouth
difference() {
  translate([-radius/2+0.5,-87,0]) cube([radius-1, mouth_depth, height]);
  translate([-92/2,-87,thickness]) cube([92, mouth_depth, height]);
}

// right foot
difference() {
  hull() {
    translate([79, 79,0]) cylinder(h=thickness, d=8);
    cylinder(h=thickness, r=radius/1.3);
  }
  cylinder(h=thickness, r=radius);
  translate([79, 79,0]) cylinder(h=thickness, d=4);
  translate([79, 79,0]) rotate([0,0,45]) cube([10,10,thickness]);
  translate([79, 79,0]) rotate([0,0,-45]) cube([10,10,thickness]);
}

// left foot
difference() {
  hull() {
    translate([79, -79,0]) cylinder(h=thickness, d=8);
    cylinder(h=thickness, r=radius/1.3);
  }
  cylinder(h=thickness, r=radius);
  translate([79, -79,0]) cylinder(h=thickness, d=4);
  translate([79, -79,0]) rotate([0,0,-45]) cube([10,10,thickness]);
  translate([79, -79,0]) rotate([0,0,-135]) cube([10,10,thickness]);
}
