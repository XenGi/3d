$fn = 64;

acrylic = 3;
width = 10;
height = 10;
ptfe = 4.5;
walls = 2;

difference() {
    hull() {
        translate([width/2, 2*walls+acrylic+ptfe/2, 0]) cylinder(h=height, d=ptfe+2*walls);
        cube([width, 2*walls+acrylic, height]);
    }
    translate([width/2, 2*walls+acrylic+ptfe/2, 0])  cylinder(h=height, d=ptfe);
    translate([0,walls,0]) cube([width, acrylic, height-walls]);
}
