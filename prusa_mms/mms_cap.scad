$fn = 64;

tubes = 4;
wall = 5;

difference() {
    cylinder(h=7, d=tubes+2*wall);
    cylinder(h=5, d=tubes);
}
