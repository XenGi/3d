$fn = 64;

module spool() {
  rotate([0,90,0]) difference() {
    union() {
      cylinder(h=2, d=200);
      cylinder(h=63, d=100);
      translate([0,0,63]) cylinder(h=2, d=200);
    }
    cylinder(h=65, d=55);
  }
}

translate([0,0,110]) {
  // rods
  color("silver") {
    translate([-234,-79,-79]) rotate([0,90,0]) cylinder(h=460, d=4);
    translate([-234,79,-79]) rotate([0,90,0]) cylinder(h=460, d=4);
  }

  // buffer
  color("yellow") translate([57,0,0]) rotate([0,90,180]) {
    import("mms_buffer_a.stl");
    translate([0,0,57]) rotate([0,180,0]) import("mms_buffer_b.stl");
    translate([0,0,11]) import("mms_buffer_spacer.stl");
    translate([0,0,22]) import("mms_buffer_spacer.stl");
    translate([0,0,33]) import("mms_buffer_spacer.stl");
    translate([0,0,44]) import("mms_buffer_spacer.stl");
  }

  // filament
  color("black") translate([65,0,0]) spool();
  color("white") translate([135,0,0]) spool();
  color("cyan") translate([-70,0,0]) spool();
  color("lime") translate([-140,0,0]) spool();
  color("magenta") translate([-210,0,0]) spool();
}

// shelf
color("DarkSlateGray") {
  translate([225,150,0]) rotate([90,0,-90]) import("mms_shelf_r.stl");
  translate([-233,150,0]) rotate([90,0,-90]) import("mms_shelf_l.stl");
  translate([-230,150,0]) rotate([90,0,0]) import("mms_shelf_back.stl");
  translate([222,-116,0]) rotate([90,0,180]) import("mms_shelf_front.stl");
}
