$fn = 64;

include <panel.scad>

ref = "R4";
radius = 100;
wall = 2;
thickness = 1;
mouth_depth = 5;
height = 5*10+6*thickness;  // 5 chambers plus 6 walls
tab_width = 9;

difference() {
  panel(thickness, radius-wall, thickness);
  translate([0,-5,thickness/2]) linear_extrude(thickness/2) text(ref, size=10, halign="center");
  translate([-radius,-radius-87+mouth_depth,0]) cube([2*radius, radius, height]);
}
translate([-92/2,-87+mouth_depth,0]) cube([92, mouth_depth, 1]);
// teeth
translate([-radius-wall+1,-tab_width/2,0]) cube([2*wall, tab_width, 1]);
translate([radius-wall-1,-tab_width/2,0]) cube([2*wall, tab_width, 1]);
translate([-tab_width/2,radius-wall-1,0]) cube([tab_width, 2*wall, 1]);
