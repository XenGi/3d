$fn = 64;

ref = "R4";
thickness = 3;
width = 342;
height = 110;

difference() {
    union() {
        linear_extrude(thickness) polygon([[0,0], [0,height], [266,height], [266,108-22], [width,40], [width,0]]);
        translate([93,0,thickness]) rotate([270,0,0]) cylinder(h=10,d=6);
        translate([195,0,thickness]) rotate([270,0,0]) cylinder(h=10,d=6);
        translate([315,0,thickness]) rotate([270,0,0]) cylinder(h=10,d=6);
    }
    translate([93,0,thickness]) rotate([270,0,0]) cylinder(h=10,d=3);
    translate([93,10,thickness]) rotate([270,0,0]) cylinder(h=3,d=6);
    translate([195,0,thickness]) rotate([270,0,0]) cylinder(h=10,d=3);
    translate([195,10,thickness]) rotate([270,0,0]) cylinder(h=3,d=6);
    translate([315,0,thickness]) rotate([270,0,0]) cylinder(h=10,d=3);
    translate([315,10,thickness]) rotate([270,0,0]) cylinder(h=3,d=6);
    translate([width/2,height/2,thickness-0.5]) linear_extrude(0.5) text(ref, size=10, halign="center");
    // holes for rods
    translate([71,31,0]) cylinder(h=thickness, d=4);
    translate([229,31,0]) cylinder(h=thickness, d=4);
    // holes for front and back connection
    translate([3,20,0]) cylinder(h=thickness, d=3);
    translate([3,height-20,0]) cylinder(h=thickness, d=3);
    translate([266-3,20,0]) cylinder(h=thickness, d=3);
    translate([266-3,height-20,0]) cylinder(h=thickness, d=3);
}
