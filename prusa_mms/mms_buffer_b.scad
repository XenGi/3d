$fn = 64;

include <panel.scad>

ref = "R4";
radius = 100;
wall = 2;
thickness = 1;
height = 5;
mouth_depth = 5;

difference() {
  union() {
    panel(1, radius, thickness);
    translate([0,0,1]) cylinder(h=height-1, r=radius-wall-0.1); // 0.1 for printer tolerance
  }
  translate([0,-5,thickness/2]) linear_extrude(thickness/2) text(ref, size=10, halign="center");
  translate([0,0,1]) {
    cylinder(h=height, r=radius-wall*2);
    translate([25, 25, 0]) cube([radius, radius, height]);
    translate([-25, 25, 0]) rotate([0,0,90]) cube([radius, radius, height]);
    translate([-radius/2-15, -25, 0]) rotate([0,0,180]) cube([radius, radius, height]);
    translate([radius/2+15, -25, 0]) rotate([0,0,270]) cube([radius, radius, height]);
    // mouth
    translate([-radius/2+0.5,-87,0]) cube([radius-1, mouth_depth, height]);
  }
  translate([-radius,-radius-87,0]) cube([2*radius, radius, height]);
}

translate([-radius/2+0.5,-87,0]) cube([radius-1, mouth_depth, 1]);

// right foot
difference() {
  hull() {
    translate([-79, 79,0]) cylinder(h=thickness, d=8);
    cylinder(h=thickness, r=radius/1.3);
  }
  cylinder(h=thickness, r=radius);
  translate([-79, 79,0]) cylinder(h=thickness, d=4);
  translate([-79, 79,0]) rotate([0,0,135]) cube([10,10,1]);
  translate([-79, 79,0]) rotate([0,0,45]) cube([10,10,1]);
}

// left foot
difference() {
  hull() {
    translate([-79, -79,0]) cylinder(h=thickness, d=8);
    cylinder(h=thickness, r=radius/1.3);
  }
  cylinder(h=thickness, r=radius);
  translate([-79, -79,0]) cylinder(h=thickness, d=4);
  translate([-79, -79,0]) rotate([0,0,-135]) cube([10,10,1]);
  translate([-79, -79,0]) rotate([0,0,135]) cube([10,10,1]);
}
