module panel(height, radius, thickness) {
  difference() {
    cylinder(h=height, r=radius);
    cylinder(h=thickness, r=radius-10);
  }
  intersection() {
    cylinder(h=thickness, r=radius);
    union() {
      translate([70,-radius,0]) cube([10, 2*radius, thickness]);
      translate([55,-radius,0]) cube([10, 2*radius, thickness]);
      translate([40,-radius,0]) cube([10, 2*radius, thickness]);
      translate([25,-radius,0]) cube([10, 2*radius, thickness]);
      translate([10,-radius,0]) cube([10, 2*radius, thickness]);
      translate([-5,-radius,0]) cube([10, 2*radius, thickness]);
      translate([-20,-radius,0]) cube([10, 2*radius, thickness]);
      translate([-35,-radius,0]) cube([10, 2*radius, thickness]);
      translate([-50,-radius,0]) cube([10, 2*radius, thickness]);
      translate([-65,-radius,0]) cube([10, 2*radius, thickness]);
      translate([-65,-radius,0]) cube([10, 2*radius, thickness]);
      translate([-80,-radius,0]) cube([10, 2*radius, thickness]);
    }
  }
  cylinder(h=thickness, r=15);
}
