$fn = 64;

ref = "R4";
thickness = 3;
width = 452;
height= 110;

difference() {
    union() {
        cube([width,height,thickness]);
        translate([166,0,thickness]) rotate([270,0,0]) cylinder(h=10,d=6);
        translate([286,0,thickness]) rotate([270,0,0]) cylinder(h=10,d=6);
        
        translate([0,20,thickness]) rotate([0,90,0]) cylinder(h=10,d=6);
        translate([0,height-20,thickness]) rotate([0,90,0]) cylinder(h=10,d=6);
        translate([width-10,20,thickness]) rotate([0,90,0]) cylinder(h=10,d=6);
        translate([width-10,height-20,thickness]) rotate([0,90,0]) cylinder(h=10,d=6);
    }
    translate([166,0,thickness]) rotate([270,0,0]) cylinder(h=10,d=3);
    translate([166,10,thickness]) rotate([270,0,0]) cylinder(h=3,d=6);
    translate([286,0,thickness]) rotate([270,0,0]) cylinder(h=10,d=3);
    translate([286,10,thickness]) rotate([270,0,0]) cylinder(h=3,d=6);
    
    translate([0,20,thickness]) rotate([0,90,0]) cylinder(h=10,d=3);
    translate([10,20,thickness]) rotate([0,90,0]) cylinder(h=3,d=6);
    translate([0,height-20,thickness]) rotate([0,90,0]) cylinder(h=10,d=3);
    translate([10,height-20,thickness]) rotate([0,90,0]) cylinder(h=3,d=6);
    translate([width-10,20,thickness]) rotate([0,90,0]) cylinder(h=10,d=3);
    translate([width-13,20,thickness]) rotate([0,90,0]) cylinder(h=3,d=6);
    translate([width-10,height-20,thickness]) rotate([0,90,0]) cylinder(h=10,d=3);
    translate([width-13,height-20,thickness]) rotate([0,90,0]) cylinder(h=3,d=6);
    
    translate([width/2,height/2,thickness-0.5]) linear_extrude(0.5) text(ref, size=10, halign="center");
}
