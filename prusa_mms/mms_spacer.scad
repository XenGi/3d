$fn = 64;

rod = 4.1; // 4mm rod plus wiggle room
thickness = 2;
wall = 2;
hole_dist = 161;
height = 44;

difference() {
    hull() {
        cylinder(h=thickness, d=rod+wall);
        translate([hole_dist,0,0]) cylinder(h=thickness, d=rod+wall);
        translate([-10,-height,-thickness]) cube([10, 10, 3*thickness]);
        translate([hole_dist,-height,-thickness]) cube([10, 10, 3*thickness]);
    }
    translate([0,0,-thickness]) {
        cylinder(h=3*thickness, d=rod);
        translate([hole_dist,0,0]) cylinder(h=3*thickness, d=rod);
    }
    translate([10, -5, -thickness]) {
        hull() {
            cylinder(h=3*thickness,d=4);
            translate([-10,-height+15,0]) cylinder(h=3*thickness,d=4);
            translate([10,-height+15,0]) cylinder(h=3*thickness,d=4);
        }
        translate([28,0,0]) hull() {
            cylinder(h=3*thickness,d=4);
            translate([-10,-height+15,0]) cylinder(h=3*thickness,d=4);
            translate([10,-height+15,0]) cylinder(h=3*thickness,d=4);
        }
        translate([56,0,0]) hull() {
            cylinder(h=3*thickness,d=4);
            translate([-10,-height+15,0]) cylinder(h=3*thickness,d=4);
            translate([10,-height+15,0]) cylinder(h=3*thickness,d=4);
        }
        translate([hole_dist-76,0,0]) hull() {
            cylinder(h=3*thickness,d=4);
            translate([-10,-height+15,0]) cylinder(h=3*thickness,d=4);
            translate([10,-height+15,0]) cylinder(h=3*thickness,d=4);
        }
        translate([hole_dist-48,0,0]) hull() {
            cylinder(h=3*thickness,d=4);
            translate([-10,-height+15,0]) cylinder(h=3*thickness,d=4);
            translate([10,-height+15,0]) cylinder(h=3*thickness,d=4);
        }
        translate([hole_dist-20,0,0]) hull() {
            cylinder(h=3*thickness,d=4);
            translate([-10,-height+15,0]) cylinder(h=3*thickness,d=4);
            translate([10,-height+15,0]) cylinder(h=3*thickness,d=4);
        }
    }
}
