$fn = 64;

thickness = 3;
wall = 1.6;
width = 20;
height = 110;

difference() {
    cube([width+thickness+2*wall, thickness+2*wall, height]);
    translate([+thickness+2*wall,wall,0]) cube([width, thickness, height]);
}
difference() {
    cube([thickness+2*wall, width+thickness+2*wall, height]);
    translate([wall,thickness+2*wall,0]) cube([thickness, width, height]);
}
difference() {
    translate([0,-width,0]) cube([thickness+2*wall, width+thickness+2*wall, height]);
    translate([wall,-width,0]) cube([thickness, width, height]);
}
