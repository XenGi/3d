$fn = 64;

inner = 3;
outer = 7;
length = 10;
wall = (outer-inner)/2;
height = 10.5;
screw = 3;
screw_head = 6;
bottom = 1.8+wall;


difference() {
    union() {
        // bottom
        cube([length,outer,bottom]);
        // walls
        cube([length, wall, height]);
        translate([0,outer-wall,0]) cube([length, wall, height]);
    }
    translate([length/2,outer/2,0]) cylinder(h=bottom, d=screw);
    translate([length/2,outer/2,wall]) cylinder(h=height, d=screw_head);
}

translate([-length/2,outer/2-inner/2,wall]) cube([length/2,inner,bottom-wall]);
translate([length,outer/2-inner/2,wall]) cube([length/2,inner,bottom-wall]);