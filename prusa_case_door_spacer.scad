$fn = 64;

corner=3;
width=45;
height=20;
thickness=3;
holes=7;
hole_distance=24.5;

space1 = 8;
space2 = 6.5;


difference() {
  translate([corner,corner,0]) hull() {
    cylinder(h=thickness,r=corner);
    translate([width-2*corner,0,0]) cylinder(h=thickness,r=corner);
    translate([0,height-2*corner,0]) cylinder(h=thickness,r=corner);
    translate([width-2*corner,height-2*corner,0]) cylinder(h=thickness,r=corner);
  }
  translate([width/2-hole_distance/2,height/2,0]) cylinder(h=thickness,d=holes);
  translate([width/2+hole_distance/2,height/2,0]) cylinder(h=thickness,d=holes);
}

translate([4,height/2-space1/2,thickness]) hull() {
  cube([1,space1,3]);
  translate([-2,space1/2,0]) cylinder(h=2,d=3);
}
translate([width-4-1,height/2-space1/2,thickness]) hull() {
  cube([1,space1,3]);
  translate([2+1,space1/2,0]) cylinder(h=2,d=3);
}

translate([15,height/2-space2/2,thickness]) hull() {
  cube([1,space2,3]);
  translate([2,space2/2,0]) cylinder(h=2,d=3);
}
translate([width-15-1,height/2-space2/2,thickness]) hull() {
  cube([1,space2,3]);
  translate([-2,space2/2,0]) cylinder(h=2,d=3);
}
